# tthbb13\_nano\_standalone

## Instructions 

### Installation
1. Clone the nanoAOD-tools into `$CMSSW_BASE/src/PhysicsTools/NanoAODTools`. Use something like: `git clone $REPO --branch $BRANCH  PhysicsTools/NanoAODTools` from the src folder
2. Run `scram b` so that all the relevant path are added the evironment

**nanoAOD-Tools:**

- Official Repo: [cms-nanoAOD/nanoAOD-tools](https://github.com/cms-nanoAOD/nanoAOD-tools)
- My fork: [kschweiger/nanoAOD-tools](https://github.com/kschweiger/nanoAOD-tools)


### Runnning the postprocessing

- `nano_postproc.py`: Main script to run the postprocessing
- `nano_config.py`: Configuration for the postprocessor modules

**Minimal example:**

```
python nano_postproc.py \
--input path/to/nanoAOD/file.root
--era 80X # Use 80X for 2016, 94Xv2 for 2017 and 102Xv1 for 2018
--noFriend # Pass this, otherwise a friend tree will be created
```
