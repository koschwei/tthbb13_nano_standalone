import os
#Creates the era-dependent configuration for the NanoAOD postprocessor
from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetHelperRun2 import createJMECorrector

class NanoConfig:
    def __init__(self, setEra, jec=False, btag=False, pu=False, isData=False, run=None,
                 tHqReweighting=False,tHWReweighting=False,ttHReweighting=False):
        # if run is not None and not isData:
        #     raise RuntimeError("Only set run for data!")
        # if run is None and isData:
        #     raise RuntimeError("Run needs to be set if passing isData=True")
        ###################################################################################################
        if setEra == "80X":
            self.eraMC = "Run2_2016,run2_nanoAOD_94X2016"
            self.eraData = "Run2_2016,run2_miniAOD_80XLegacy"
            self.conditionsMC = "102X_mcRun2_asymptotic_v7"
            self.conditionsData = "auto:run2_data_relval"
            self.eraBtagSF = "2016"
            self.algoBtag = "csvv2"
            self.jecMC = "Summer16_07Aug2017_V11_MC"
            self.jecData = "Summer16_07Aug2017_V11_DATA"
            self.redoJEC = False
            self.redoJECData = False
            self.increaseHEMUnc = False
            self.puWeight = 'puAutoWeight_2016'
            print("Using CMSSW 80X")
        #For RunIIFall17MiniAOD we need v1
        elif setEra == "94Xv1":
            self.eraMC = "Run2_2017,run2_nanoAOD_94XMiniAODv1"
            self.eraData = "Run2_2017,run2_nanoAOD_94XMiniAODv1"
            self.conditionsMC = "94X_mc2017_realistic_v13"
            self.conditionsData = "94X_dataRun2_ReReco_EOY17_v6"
            self.eraBtagSF = "2017"
            self.algoBtag = "deepcsv"
            self.jecMC = "Fall17_17Nov2017_V32_MC"
            self.jecData = "Fall17_17Nov2017_V32_DATA"
            self.redoJEC = False
            self.redoJECData = False
            self.increaseHEMUnc = False
            self.puWeight = 'puAutoWeight_2017'
            print("Using CMSSW 94X with v1 era")
        elif setEra == "94Xv2":
            self.eraMC = "Run2_2017,run2_nanoAOD_94XMiniAODv2"
            self.eraData = "Run2_2017,run2_nanoAOD_94XMiniAODv2"
            self.conditionsMC = "102X_mc2017_realistic_v7"
            self.conditionsData = "94X_dataRun2_ReReco_EOY17_v6"
            self.eraBtagSF = "2017"
            self.jecMC = "Fall17_17Nov2017_V32_MC"
            self.jecData = "Fall17_17Nov2017_V32_DATA"
            self.algoBtag = "deepcsv"
            self.redoJEC = False
            self.redoJECData = False
            self.increaseHEMUnc = False
            self.puWeight = 'puAutoWeight_2017'
            print("Using CMSSW 94X with v2 era")
        elif setEra == "102Xv1":
            self.eraMC = "Run2_2018,run2_nanoAOD_102Xv1"
            self.eraData = "Run2_2018,run2_nanoAOD_102Xv1"
            self.conditionsMC = "102X_upgrade2018_realistic_v19"
            self.conditionsData = "102X_dataRun2_Sep2018Rereco_v1"
            self.jecMC = "Autumn18_V19_MC"
            self.jecData = "Autumn18_V19_DATA"
            # self.jecMC = "Fall17_17Nov2017_V32_M<C"
            # self.jecData = "Fall17_17Nov2017_V32_DATA"
            self.eraBtagSF = "2018"
            self.algoBtag = "deepcsv"
            self.redoJEC = True
            self.redoJECData = True
            self.increaseHEMUnc = True
            self.puWeight = 'puAutoWeight_2018'
            print("Using CMSSW 102X with v2 era")

            
        imports = []
        # if jec:
        #     if not isData:
        #         imports += [
        #             ('PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetUncertainties', 'jetmetUncertaintiesProducer'),
        #         ]
        #     else:
        #         imports += [
        #             ('PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetRecalib', 'jetRecalib')
        #         ]

        if btag:
            imports += [
                ('PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer','btagSFProducer')
            ]
        if pu:
            imports += [
                ('PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer',self.puWeight)
            ]
            
        if tHqReweighting:
            imports += [
                ('PhysicsTools.NanoAODTools.postprocessing.modules.specific.tH_reweighting',"tHq_reweigther")
            ]
        if tHWReweighting:
            imports += [
                ('PhysicsTools.NanoAODTools.postprocessing.modules.specific.tH_reweighting',"tHW_reweigther")
            ]
        if ttHReweighting:
            imports += [
                ('PhysicsTools.NanoAODTools.postprocessing.modules.specific.tH_reweighting',"ttH_reweigther")
            ]
            
            
        ## Figure out which run should be corrected in data
        # if isData:
        #     if setEra == "80X":
        #         if run in ["B", "C", "D"]:
        #             jecGT = "Summer16_07Aug2017BCD_V11_DATA"
        #         elif run in ["E", "F"]:
        #             jecGT = "Summer16_07Aug2017EF_V11_DATA"
        #         elif run in ["G", "H"]:
        #             jecGT = "Summer16_07Aug2017GH_V11_DATA"
        #         else:
        #             raise RuntimeError("Unsupported run %s for era %s"%(run, setEra))
        #     elif setEra == "94Xv2":
        #         if run in ["B", "C", "F"]:
        #             jecGT = "Fall17_17Nov2017{0}_V32_DATA".format(run)
        #         elif run in ["D", "E"]:
        #             jecGT = "Fall17_17Nov2017DE_V32_DATA"
        #         else:
        #             raise RuntimeError("Unsupported run %s for era %s"%(run, setEra))
        #     elif setEra == "102Xv1":
        #         if run in ["A","B","C","D"]:
        #             jecGT = "Autumn18_Run{0}_V19_DATA".format(run)
        #         else:
        #             raise RuntimeError("Unsupported run %s for era %s"%(run, setEra))
        #     else:
        #         raise RuntimeError("Unsupported era for data correction")
        #     print "Using {0} as GT for jetRecalib".format(jecGT)
            
        self.json = None #Intended use: Skimming 
        self.cuts = None #Intended use: Skimming
        self.branchsel = None

        from importlib import import_module
        import sys

        
        #Imports the various nanoAOD postprocessor modules
        self.modules = []
        for mod, names in imports:
            import_module(mod)
            obj = sys.modules[mod]
            selnames = names.split(",")
            for name in dir(obj):
                if name[0] == "_": continue
                if name in selnames:
                    print("Loading %s from %s " % (name, mod))
                    if name == "btagSFProducer":
                        self.modules.append(getattr(obj,name)(self.eraBtagSF, self.algoBtag))
                    # elif name == "jetmetUncertaintiesProducer" and not isData:
                    #     for itypeJet in ['AK4PFchs']:
                    #     #for itypeJet in ['AK4PFchs', 'AK4PFPuppi', 'AK8PFPuppi']:
                    #         self.modules.append(getattr(obj,name)(self.eraBtagSF, self.jecMC, jesUncertainties=['All'], jetType=itypeJet, redoJEC=self.redoJEC))
                    # elif name == "jetRecalib" and isData:
                    #     for itypeJet in ['AK4PFchs']:
                    #     #for itypeJet in ['AK4PFchs', 'AK4PFPuppi', 'AK8PFPuppi']:
                    #         self.modules.append(getattr(obj,name)(jecGT, self.jecData, jetType=itypeJet, redoJEC=self.redoJEC))
                    else:
                        print(name)
                        self.modules.append(getattr(obj,name)())
        if jec:
            self.modules.append(
                createJMECorrector(
                    isMC = (not isData),
                    dataYear = self.eraBtagSF,
                    runPeriod = run,
                    jesUncert = "All",
                    redojec = self.redoJEC,
                    #increaseHEMUnc = self.increaseHEMUnc,
                )()
            )
            
        print("======================================================================")
        print("====================== CONFIG SETTINGS ===============================")
        for attribute in self.__dict__:
            if attribute != "modules":
                print("   ",attribute,"=",getattr(self, attribute))
        print("----------------------------------------------------------------------")
        print("    Will use modules:")
        for mod in self.modules:
            print("       ", mod)
        print("======================================================================")
            
            
